README.txt
____________________


DESCRIPTION
____________________

Module provides outputting private messages through the views. The idea was create block with recent private messages like it done for the comments "recent comments". You may create your own views and config it as you wish, for example you may add field subject and it will lead to the thread page to which message is belongs, you may add filter only for new messages and not deleted.


REQUIREMENTS
____________________

Privatemsg module 7.x-1.4


INSTALLATION
____________________

To install this module, do the following:

1. Extract the tar ball that you downloaded from Drupal.org.

2. Upload the privatemsg_message_views directory and all its contents to your modules directory.

3. Visit admin/modules and enable the "Privatemsg message views". This module can be found within the "Mail" fieldset.


CONFIGURATION
____________________

To configure this module do the following:

1. Go to Structure -> Views (admin/structure/views) and create your own private messages views or use an existing one.
